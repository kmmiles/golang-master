FROM golang:1.10.3-stretch AS builder
RUN git clone https://go.googlesource.com/go /golang-master -b master && \
        cd /golang-master/src && \
        env GOROOT_BOOTSTRAP=/usr/local/go ./make.bash
WORKDIR /golang-master

FROM debian:stretch-slim AS runner
RUN mkdir -p /usr/local/go
COPY --from=builder /golang-master/bin /usr/local/go/bin
COPY --from=builder /golang-master/api /usr/local/go/api
COPY --from=builder /golang-master/lib /usr/local/go/lib
COPY --from=builder /golang-master/misc /usr/local/go/misc
COPY --from=builder /golang-master/pkg /usr/local/go/pkg
ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV PATH $GOPATH/bin:$GOROOT/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH
